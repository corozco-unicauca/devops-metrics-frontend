import { OnInit, Component, ElementRef, ViewChild } from "@angular/core";
import { EmpresaService } from "src/app/services/empresa.service";
import { ActivatedRoute, Router } from "@angular/router";
import { Empresa } from "src/app/models/empresa";
import { Evaluacion } from "src/app/models/evaluacion";
import { Chart } from "chart.js";

@Component({
  selector: "app-detalle",
  templateUrl: "./detalle.page.html",
  styleUrls: ["./detalle.page.scss"],
})
export class DetallePage implements OnInit {
  @ViewChild("evalHistoricoChart") private chartHistorico: ElementRef;

  lineChart: any;

  empresa: Empresa;
  evaluaciones: Evaluacion[] = [];

  constructor(
    private empresaService: EmpresaService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.cargar();
  }

  cargar(): void {
    const id = this.activatedRoute.snapshot.params.id;
    this.empresaService.detalle(id).subscribe(
      (data) => {
        this.empresa = data;
      },
      (err) => {
        this.volver();
      }
    );

    this.empresaService.evaluaciones(id).subscribe(
      (data) => {
        this.evaluaciones = data;

        let labels = this.evaluaciones.map((foo) => foo.fechaCreacion);
        let dataChart = this.evaluaciones.map((foo) => foo.resultadoEvaluacion);
        this.draw("line", this.chartHistorico, labels, dataChart);
      },
      (err) => {
        this.volver();
      }
    );
  }

  volver(): void {
    const redirectUrl = "/admin";
    this.router.navigate([redirectUrl]);
  }

  verDetalle(idEvaluacion: number) {
    this.router.navigate([`detalle/evaluacion/${idEvaluacion}`]).then(() => {});
  }

  download(id: number) {
    console.log('Start download');
    this.empresaService.download(id);
    console.log('End download');
  }

  draw(chartType: string, chart: ElementRef, labelList: any, dataList: any) {
    this.lineChart = new Chart(chart.nativeElement, {
      type: chartType,
      data: {
        labels: labelList,
        datasets: [
          {
            label: "Resultado",
            data: dataList,
            fill: false,
            borderColor: "rgb(75, 192, 192)",
          },
        ],
      },
    });
  }
}
