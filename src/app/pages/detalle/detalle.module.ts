import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { DetallePageRoutingModule } from "./detalle-routing.module";

import { DetallePage } from "./detalle.page";
import { ComponentsModule } from "src/app/components/components.module";
import { Ng2GoogleChartsModule } from "ng2-google-charts";

@NgModule({
  imports: [
    Ng2GoogleChartsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    DetallePageRoutingModule,
    ComponentsModule,
  ],
  declarations: [DetallePage],
})
export class DetallePageModule {}
