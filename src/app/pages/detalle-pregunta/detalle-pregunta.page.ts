import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { DetallePreguntas } from "src/app/models/detalle-preguntas";
import { EmpresaService } from "src/app/services/empresa.service";

@Component({
  selector: "app-detalle-pregunta",
  templateUrl: "./detalle-pregunta.page.html",
  styleUrls: ["./detalle-pregunta.page.scss"],
})
export class DetallePreguntaPage implements OnInit {
  resPreguntas: DetallePreguntas[];

  constructor(
    private empresaService: EmpresaService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    let queryValue: string = this.activatedRoute.snapshot.params.id;
    let idPractica: number = parseInt(queryValue.split("?")[0]);
    let idEvaluacion: number = parseInt(queryValue.split("?")[1]);

    this.empresaService.detallePreguntas(idEvaluacion, idPractica).subscribe(
      (data) => {
        this.resPreguntas = data;
      },
      (err) => {
        this.volver();
      }
    );
  }

  volver() {}
}
