import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { DetallePreguntaPage } from "./detalle-pregunta.page";

const routes: Routes = [
  {
    path: "",
    component: DetallePreguntaPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetallePreguntaPageRoutingModule {}
