import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { EmpresaService } from "src/app/services/empresa.service";
import { Router } from "@angular/router";
import { Empresa } from "src/app/models/empresa";
import { ToastController } from "@ionic/angular";

@Component({
  selector: "app-admin",
  templateUrl: "./admin.page.html",
  styleUrls: ["./admin.page.scss"],
})
export class AdminPage implements OnInit {
  empresas: Empresa[] = [];

  private formData: FormData;
  public uploadDisabled: Boolean = true;

  @ViewChild("uploadFile")
  myInputVariable: ElementRef;

  constructor(
    private empresaService: EmpresaService,
    private toastController: ToastController,
    private router: Router
  ) {}

  ngOnInit() {
    this.cargar();
  }

  ionViewWillEnter() {
    this.cargar();
  }

  cargar(): void {
    this.empresaService.lista().subscribe(
      (data) => {
        this.empresas = data;
      },
      (err) => {
        console.log(err);
      }
    );
  }

  async presentToast(mensaje: string) {
    const toast = await this.toastController.create({
      message: mensaje,
      duration: 2000,
      position: "top",
    });
    toast.present();
  }

  onFileChange(fileChangeEvent) {
    let file: File = fileChangeEvent.target.files[0];
    if (null != file) {
      this.formData = new FormData();
      this.formData.append("file", file, file.name);
    }
    this.uploadDisabled = null == file;
  }

  upload() {
    if (null != this.formData) {
      this.empresaService.upload(this.formData).subscribe(
        (data) => {
          let current: Empresa = data;
          let exists: Boolean =
            null != this.empresas.find((aux) => aux.nombre == current.nombre);
          if (!exists) {
            this.empresas.push(current);
          }
          this.reset();
          this.presentToast("La evaluación se ha registrado exitosamente");
        },
        (error) => {
          console.log(error);
        }
      );
    } else {
      console.log("Not loaded file");
    }
  }

  verDetalle(id: number) {
    this.router.navigate([`detalle/${id}`]).then(() => {});
  }
  reset() {
    this.myInputVariable.nativeElement.value = "";
    this.uploadDisabled = true;
  }
}
