import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { DetalleEvaluacionPageRoutingModule } from "./detalle-evaluacion-routing.module";

import { DetalleEvaluacionPage } from "./detalle-evaluacion.page";
import { ComponentsModule } from "src/app/components/components.module";
import { Ng2GoogleChartsModule } from "ng2-google-charts";

@NgModule({
  imports: [
    Ng2GoogleChartsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    DetalleEvaluacionPageRoutingModule,
    ComponentsModule,
  ],
  declarations: [DetalleEvaluacionPage],
})
export class DetalleEvaluacionPageModule {}
