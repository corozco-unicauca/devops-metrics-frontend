import { OnInit, Component, ElementRef, ViewChild } from "@angular/core";
import { EmpresaService } from "src/app/services/empresa.service";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastController } from "@ionic/angular";
import { ResultadoEvaluacion } from "src/app/models/resultado-evaluacion";
import { ResultadoPractica } from "src/app/models/resultado_practica";
import { ResultadoValor } from "src/app/models/resultado-valor";
import { ResultadoDimension } from "src/app/models/resultado-dimension";
import { Chart } from "chart.js";

@Component({
  selector: "app-detalle-evaluacion",
  templateUrl: "./detalle-evaluacion.page.html",
  styleUrls: ["./detalle-evaluacion.page.scss"],
})
export class DetalleEvaluacionPage implements OnInit {
  @ViewChild("chartFundamental") private chartFundamental: ElementRef;
  @ViewChild("chartFundamentalPond") private chartFundamentalPond: ElementRef;
  @ViewChild("chartComplementario") private chartComplementario: ElementRef;
  @ViewChild("chartComplementarioPond") private chartComplementarioPond: ElementRef;
  @ViewChild("chartDimensiones") private chartDimensiones: ElementRef;
  @ViewChild("chartDimensionesPond") private chartDimensionesPond: ElementRef;
  @ViewChild("chartValoresPond") private chartValoresPond: ElementRef;
  @ViewChild("chartValores") private chartValores: ElementRef;

  barChart: any;
  doughnutChart: any;
  lineChart: any;
  radarChart: any;
  
  // Result attributes
  evaluacion: ResultadoEvaluacion;
  resPracticasFund: ResultadoPractica[];
  resPracticasComp: ResultadoPractica[];
  resDimensiones: ResultadoDimension[];
  resValores: ResultadoValor[];

  pft: number;
  pct: number;
  pdt: number;
  pvt: number;
  id: number;

  // CONSTANTS
  puntuacionIndividual = "Puntuación individual [%]";
  puntuacionPonderada = "Puntuación ponderada [%]";

  constructor(
    private empresaService: EmpresaService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private toastController: ToastController
  ) {}

  ngOnInit() {
    this.cargar();
  }

  cargar(): void {
    this.id = this.activatedRoute.snapshot.params.id;
    this.empresaService.detalleEvaluacion(this.id).subscribe(
      (data) => {
        this.evaluacion = data;
        this.resPracticasFund = data.resultadoPracticas.filter(
          (e) => e.tipoPractica === "F"
        );
        this.resPracticasComp = data.resultadoPracticas.filter(
          (e) => e.tipoPractica === "C"
        );

        this.pft = this.evaluacion.resPracticasFund;
        this.pct = this.evaluacion.resPracticasComp;
        this.resDimensiones = data.resultadoDimensiones;
        this.pdt = this.evaluacion.resTotalDimensiones;
        this.resValores = data.resultadoValores;
        this.pvt = this.evaluacion.cumplimientoDevOps;

        this.drawPracticas();
        this.drawDimensiones();
        this.drawValores();
      },
      (err) => {
        this.volver();
      }
    );
  }

  drawPracticas() {
    let resFundIndividual = this.resPracticasFund.map(
      (foo) => foo.puntuacionIndividual
    );
    let resFundPonderado = this.resPracticasFund.map(
      (foo) => foo.puntuacionPonderada
    );
    let labelFund = this.resPracticasFund.map((foo) => foo.nombrePractica);
    let resCompIndividual = this.resPracticasComp.map(
      (foo) => foo.puntuacionIndividual
    );
    let resCompPonderado = this.resPracticasComp.map(
      (foo) => foo.puntuacionPonderada
    );
    let labelComp = this.resPracticasComp.map((foo) => foo.nombrePractica);

    this.drawChart(
      "radar",
      this.chartFundamental,
      labelFund,
      this.puntuacionIndividual,
      resFundIndividual,
      ['rgba(54, 162, 235, 0.5)', 'rgb(54, 162, 235)']
    );

    this.drawChart(
      "radar",
      this.chartFundamentalPond,
      labelFund,
      this.puntuacionPonderada,
      resFundPonderado,
      ['rgba(255, 99, 132, 0.2)', 'rgb(255, 99, 132)']
    );

    this.drawChart(
      "radar",
      this.chartComplementario,
      labelComp,
      this.puntuacionIndividual,
      resCompIndividual,
      ['rgba(54, 162, 235, 0.5)', 'rgb(54, 162, 235)']
    );

    this.drawChart(
      "radar",
      this.chartComplementarioPond,
      labelComp,
      this.puntuacionPonderada,
      resCompPonderado,
      ['rgba(255, 99, 132, 0.2)', 'rgb(255, 99, 132)']
    );
    
  }

  drawDimensiones() {
    let resDimIndividual = this.resDimensiones.map(
      (foo) => foo.puntuacionIndividual
    );
    let resDimPonderado = this.resDimensiones.map(
      (foo) => foo.puntuacionPonderada
    );
    let labelDim = this.resDimensiones.map((foo) => foo.nombreDimension);

    this.drawChart(
      "radar",
      this.chartDimensiones,
      labelDim,
      this.puntuacionIndividual,
      resDimIndividual,
      ['rgba(54, 162, 235, 0.5)', 'rgb(54, 162, 235)']
    );

    this.drawChart(
      "radar",
      this.chartDimensionesPond,
      labelDim,
      this.puntuacionPonderada,
      resDimPonderado,
      ['rgba(255, 99, 132, 0.2)', 'rgb(255, 99, 132)']
    );
  }

  drawValores() {
    let resValIndividual = this.resValores.map(
      (foo) => foo.puntuacionIndividual
    );
    let resValPonderado = this.resValores.map((foo) => foo.puntuacionPonderada);
    let labelVal = this.resValores.map((foo) => foo.nombreValor);

    this.drawChart(
      "radar",
      this.chartValores,
      labelVal,
      this.puntuacionIndividual,
      resValIndividual,
      ['rgba(54, 162, 235, 0.5)', 'rgb(54, 162, 235)']
    );

    this.drawChart(
      "radar",
      this.chartValoresPond,
      labelVal,
      this.puntuacionPonderada,
      resValPonderado,
      ['rgba(255, 99, 132, 0.2)', 'rgb(255, 99, 132)']
    );
  }

  onUpdate(): void {
    const id = this.activatedRoute.snapshot.params.id;
  }

  vaciar() {
    this.evaluacion = null;
  }

  volver() {
    this.router
      .navigate([`detalle/${this.evaluacion.idEmpresa}`])
      .then(() => {});
  }

  verDetallePreguntas(idPractica: number) {
    this.router
      .navigate([
        `detalle/evaluacion/preguntas/${idPractica}?${this.evaluacion.idEvaluacion}`,
      ])
      .then(() => {});
  }

  async presentToast(mensaje: string) {
    const toast = await this.toastController.create({
      message: mensaje,
      duration: 2000,
      position: "middle",
    });
    toast.present();
  }

  drawChart(
    chartType: string,
    chart: ElementRef,
    labelList: any,
    title : any,
    res: any,
    colors: any
  ) {
    this.barChart = new Chart(chart.nativeElement, {
      type: chartType,
      data: {
        labels: labelList,
        datasets: [
          {
            label: title,
            data: res,
            borderWidth: 1,
            backgroundColor: colors[0],
            borderColor: colors[1],
          }
        ],
      },
      options: {
        scales: {
          
        },
      },
    });
  }
}
