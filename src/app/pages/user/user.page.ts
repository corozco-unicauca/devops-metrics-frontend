import { Component, OnInit } from '@angular/core';
import { EmpresaService } from 'src/app/services/empresa.service';
import { Empresa } from 'src/app/models/empresa';

@Component({
  selector: 'app-user',
  templateUrl: './user.page.html',
  styleUrls: ['./user.page.scss'],
})
export class UserPage implements OnInit {

  empresas: Empresa[] = [];

  constructor(
    private empresaService: EmpresaService
  ) { }

  ngOnInit() {
    this.cargar();
  }

  cargar(): void {
    this.empresaService.lista().subscribe(
      (data) => {
        this.empresas = data;
      },
      (err) => {
        console.log(err);
      }
    );
  }

}
