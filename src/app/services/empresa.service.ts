import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from "rxjs";
import { Empresa } from "../models/empresa";
import { Evaluacion } from "../models/evaluacion";
import { ResultadoEvaluacion } from "../models/resultado-evaluacion";
import { DetallePreguntas } from "../models/detalle-preguntas";

@Injectable({
  providedIn: "root",
})
export class EmpresaService {
  empresaURL = "http://localhost:8001/admin/";
  evaluacionURL = "http://localhost:8001/metrics/";

  constructor(private httpClient: HttpClient) { }

  public lista(): Observable<Empresa[]> {
    return this.httpClient.get<Empresa[]>(this.empresaURL + "empresas");
  }

  public detalle(id: number): Observable<Empresa> {
    return this.httpClient.get<Empresa>(this.empresaURL + `detalle/${id}`);
  }

  public evaluaciones(id: number): Observable<Evaluacion[]> {
    return this.httpClient.get<Evaluacion[]>(
      this.empresaURL + `detalle/evaluaciones/${id}`
    );
  }

  public detalleEvaluacion(id: number): Observable<ResultadoEvaluacion> {
    return this.httpClient.get<ResultadoEvaluacion>(
      this.evaluacionURL + `evaluacion/${id}`
    );
  }

  public detallePreguntas(
    idEvaluacion: number,
    idPractica: number
  ): Observable<DetallePreguntas[]> {
    return this.httpClient.post<DetallePreguntas[]>(
      this.evaluacionURL + `preguntas`,
      { idEvaluacion: idEvaluacion, idPractica: idPractica }
    );
  }

  public upload(formData: FormData): Observable<Empresa> {
    return this.httpClient.post<Empresa>(
      this.evaluacionURL + `create`,
      formData
    );
  }

  public download(id: number) {
    const url = this.evaluacionURL + `evaluacion/download/${id}`

    const headers = new HttpHeaders().set('Accept', 'application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    this.httpClient.get(url, { headers, responseType: 'blob' }).subscribe(response => {
      const blob = new Blob([response], { type: 'application/vnd.ms-excel' }); // or 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      const url = URL.createObjectURL(blob);
      const link = document.createElement('a');
      link.href = url;
      link.download = `file.xlsx`; // or 'file.xls'
      link.click();
    });
  }
}
