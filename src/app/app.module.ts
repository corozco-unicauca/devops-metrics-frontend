import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ComponentsModule } from './components/components.module';

import { HttpClientModule } from '@angular/common/http';
import { interceptorProvider } from './interceptors/empresa-interceptor.service';
import { PDFGenerator } from '@ionic-native/pdf-generator/ngx';


@NgModule({
    declarations: [AppComponent],
    imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, ComponentsModule, HttpClientModule],
    providers: [
        StatusBar,
        SplashScreen,
        PDFGenerator,
        { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
        interceptorProvider
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
