export class DownloadFileVO {
    filename: String;
    file: Blob;

    constructor(
        filename: String,
        file: Blob
    ) {
        this.filename = filename;
        this.file = file;
    }
}