export class ResultadoDimension {
  puntuacionIndividual: number;
  puntuacionPonderada: number;
  codigoDimension: string;
  nombreDimension: string;

  constructor(
    puntuacionIndividual: number,
    puntuacionPonderada: number,
    codigoDimension: string,
    nombreDimension: string
  ) {
    this.puntuacionIndividual = puntuacionIndividual;
    this.puntuacionPonderada = puntuacionPonderada;
    this.codigoDimension = codigoDimension;
    this.nombreDimension = nombreDimension;
  }
}
