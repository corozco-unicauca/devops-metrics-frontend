export class Evaluacion {
  id?: number;
  fechaEvaluacion: Date;
  resultadoEvaluacion: string;
  gradoCumplimiento: string;
  fechaCreacion: Date;
  rutaArchivo: string;

  constructor(
    fechaEvaluacion: Date,
    resultadoEvaluacion: string,
    gradoCumplimiento: string,
    fechaCreacion: Date,
    rutaArchivo: string
  ) {
    this.fechaEvaluacion = fechaEvaluacion;
    this.resultadoEvaluacion = resultadoEvaluacion;
    this.gradoCumplimiento = gradoCumplimiento;
    this.fechaCreacion = fechaCreacion;
    this.rutaArchivo = rutaArchivo;
  }
}
