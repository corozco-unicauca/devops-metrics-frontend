export class DetallePreguntas {
  codigoPregunta: string;
  pregunta: string;
  respuestaSi: string;
  respuestaNo: string;
  observaciones: string;

  constructor(
    codigoPregunta: string,
    pregunta: string,
    respuestaSi: string,
    respuestaNo: string,
    observaciones: string
    ) {
    this.codigoPregunta = codigoPregunta;
    this.pregunta = pregunta;
    this.respuestaSi = respuestaSi;
    this.respuestaNo = respuestaNo;
    this.observaciones = observaciones;

  }
}
