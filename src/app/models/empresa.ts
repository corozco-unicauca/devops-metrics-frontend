/* eslint-disable @typescript-eslint/naming-convention */
export class Empresa {
  id?: number;
  nombre: string;
  nit: string;
  direccion: string;
  descripcion: string;
  contacto: string;
  fecha_creacion: Date;
  activo: string;

  constructor(
    nombre: string,
    nit: string,
    direccion: string,
    descripcion: string,
    contacto: string,
    fecha_creacion: Date,
    activo: boolean
  ) {
    this.nombre = nombre;
    this.direccion = direccion;
    this.nit = nit;
    this.descripcion = descripcion;
    this.contacto = contacto;
    this.fecha_creacion = fecha_creacion;
    this.activo = activo ? "Activo" : "Inactivo";
  }
}
