import { ResultadoDimension } from "./resultado-dimension";
import { ResultadoValor } from "./resultado-valor";
import { ResultadoPractica } from "./resultado_practica";

export class ResultadoEvaluacion {
  idEvaluacion: number;
  idEmpresa: number;
  nombreEmpresa: string;
  fechaEvaluacion: Date;

  resultadoPracticas: ResultadoPractica[];

  resPracticasFund: number;
  resPracticasComp: number;
  resTotalPracticas: number;

  resultadoDimensiones: ResultadoDimension[];
  resTotalDimensiones: number;

  resultadoValores: ResultadoValor[];
  cumplimientoDevOps: number;

  constructor(
    idEvaluacion: number,
    idEmpresa: number,
    nombreEmpresa: string,
    fechaEvaluacion: Date,
    resultadoPracticas: ResultadoPractica[],
    resPracticasFund: number,
    resPracticasComp: number,
    resTotalPracticas: number,
    resultadoDimensiones: ResultadoDimension[],
    resTotalDimensiones: number,
    resultadoValores: ResultadoValor[],
    cumplimientoDevOps: number
  ) {
    this.idEvaluacion = idEvaluacion;
    this.idEmpresa = idEmpresa;
    this.nombreEmpresa = nombreEmpresa;
    this.fechaEvaluacion = fechaEvaluacion;
    this.resultadoPracticas = resultadoPracticas;
    this.resPracticasFund = resPracticasFund;
    this.resPracticasComp = resPracticasComp;
    this.resTotalPracticas = resTotalPracticas;
    this.resultadoDimensiones = resultadoDimensiones;
    this.resTotalDimensiones = resTotalDimensiones;
    this.resultadoValores = resultadoValores;
    this.cumplimientoDevOps = cumplimientoDevOps;
  }
}
