export class ResultadoValor {
  puntuacionIndividual: number;
  puntuacionPonderada: number;
  codigoValor: string;
  nombreValor: string;

  constructor(
    puntuacionIndividual: number,
    puntuacionPonderada: number,
    codigoValor: string,
    nombreValor: string
  ) {
    this.puntuacionIndividual = puntuacionIndividual;
    this.puntuacionPonderada = puntuacionPonderada;
    this.codigoValor = codigoValor;
    this.nombreValor = nombreValor;
  }
}
