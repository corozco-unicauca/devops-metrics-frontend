export class ResultadoPractica {
  idPractica: number;
  puntuacionIndividual: number;
  puntuacionPonderada: number;
  codigoPractica: string;
  nombrePractica: string;
  tipoPractica: string;

  constructor(
    idPractica: number,
    puntuacionIndividual: number,
    puntuacionPonderada: number,
    codigoPractica: string,
    nombrePractica: string,
    tipoPractica: string
  ) {
    this.idPractica = idPractica;
    this.puntuacionIndividual = puntuacionIndividual;
    this.puntuacionPonderada = puntuacionPonderada;
    this.codigoPractica = codigoPractica;
    this.nombrePractica = nombrePractica;
    this.tipoPractica = tipoPractica;
  }
}
