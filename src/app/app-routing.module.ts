import { NgModule } from '@angular/core';
import { EmpresaGuard as guard } from './guards/empresa-guard.guard';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  {
    path: "home",
    loadChildren: () =>
      import("./pages/admin/admin.module").then((m) => m.AdminPageModule),
  },
  {
    path: "",
    redirectTo: "home",
    pathMatch: "full",
  },
  {
    path: "admin",
    loadChildren: () =>
      import("./pages/admin/admin.module").then((m) => m.AdminPageModule),
    canActivate: [guard],
    data: { requiredRoles: ["admin"] },
  },
  {
    path: "user",
    loadChildren: () =>
      import("./pages/user/user.module").then((m) => m.UserPageModule),
    canActivate: [guard],
    data: { requiredRoles: ["admin", "user"] },
  },
  {
    path: "login",
    loadChildren: () =>
      import("./pages/login/login.module").then((m) => m.LoginPageModule),
  },
  {
    path: "registro",
    loadChildren: () =>
      import("./pages/registro/registro.module").then(
        (m) => m.RegistroPageModule
      ),
  },
  {
    path: "detalle/:id",
    loadChildren: () =>
      import("./pages/detalle/detalle.module").then((m) => m.DetallePageModule),
  },
  {
    path: "detalle/evaluacion/:id",
    loadChildren: () =>
      import("./pages/detalle-evaluacion/detalle-evaluacion.module").then(
        (m) => m.DetalleEvaluacionPageModule
      ),
  },

  {
    path: "detalle/evaluacion/preguntas/:id",
    loadChildren: () =>
      import("./pages/detalle-pregunta/detalle-pregunta.module").then(
        (m) => m.DetallePreguntaPageModule
      ),
  },
  {
    path: "**",
    redirectTo: "home",
    pathMatch: "full",
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules, relativeLinkResolution: 'legacy' })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
