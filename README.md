# README #

* Author: PhD(c). MSc. Eng. Carlos Eduardo Orozco
* Institution: Universidad del Cauca

### What is this repository for? ###

* Project created with the objective of supporting the process to evaluate the degree of adoption of the practices, dimensions and values ​​proposed in the DevOps Model through a reporting system that allows calculating and visualizing the information of interest to a consultant or professional who seeks to evaluate DevOps in software companies.
* Version 1.0

### How do I get set up? ###

* Install NodeJS and npm: 
  * [Official installation documentation](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)
* Install Ionic: 
  * [Official installation documentation](https://ionicframework.com/docs/intro/cli)

* For local-environments execute:
```sh
rm -rf node_modules && rm package-lock.json
```  
* Execute:
```sh
npm install --force
```
* Execute:
```sh
ionic serve
```

### Who do I talk to? ###

* Carlos Eduardo Orozco (carlosorozco@unicauca.edu.co)